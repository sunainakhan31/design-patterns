public class NewMain {

    public static void main(String[] args) {
        /*
            Adpter Pattrn
        */
        CreditCardInterface targetInterface = new BankCustomer();
        targetInterface.giveBankDetails();
        System.out.print(targetInterface.getCreditCard());
        /*
            Iterator Pattrn
        */
        NamesCollectoin cmpnyRepository = new NamesCollectoin();
        for (Iterator iter = cmpnyRepository.getIterator(); iter.hasNext();) {
            String name = (String) iter.next();
            System.out.println("Name : " + name);
        }
        /*
            Prototype Pattern
        */
        Scanner in = new Scanner(System.in);
        System.out.print("Enter Employee Id: ");
        int eid = in.nextInt();
        System.out.print("Enter Employee Name: ");
        String ename = in.next();
        System.out.print("Enter Employee Designation: ");
        String edesignation = in.next();
        System.out.print("Enter Employee Address: ");
        String eaddress = in.nextLine();
        System.out.print("Enter Employee Salary: ");
        double esalary = in.nextDouble();
        EmployeeRecord e1 = new EmployeeRecord(eid, ename, edesignation, esalary, eaddress);
        e1.showRecord();
        System.out.println("");
        EmployeeRecord e2 = (EmployeeRecord) e1.getClone();
        e2.showRecord();
    }
}
//Adapter Pattern
public interface CreditCardInterface {  
    public void giveBankDetails();  
    public String getCreditCard();  
}
public class BankDetails{  
    private String bankName;  
    private String accHolderName;  
    private long accNumber;  
      
    public BankDetails(){
        
    }

    public String getBankName() {  
        return bankName;  
    }  
    public void setBankName(String bankName) {  
        this.bankName = bankName;  
    }  
    public String getAccHolderName() {  
        return accHolderName;  
    }  
    public void setAccHolderName(String accHolderName) {  
        this.accHolderName = accHolderName;  
    }  
    public long getAccNumber() {  
        return accNumber;  
    }  
    public void setAccNumber(long accNumber) {  
        this.accNumber = accNumber;  
    }  
}
public class BankCustomer extends BankDetails implements CreditCardInterface {

    Scanner in = new Scanner(System.in);

    public void giveBankDetails() {
        try {
            System.out.println("Account holder name:");
            String customername = in.next();

            System.out.println("Account number:");
            long accno = in.nextLong();

            System.out.println("Bank name :");
            String bankname = in.next();

            setAccHolderName(customername);
            setAccNumber(accno);
            setBankName(bankname);
        } catch (Exception e) {
        }
    }

    public String getCreditCard() {
        long accno = getAccNumber();
        String accholdername = getAccHolderName();
        String bname = getBankName();

        return ("The Account number " + accno + " of " + accholdername + " in " + bname + " bank is valid and authenticated for issuing the  credit card.");

    }
}
//Iterator Pattern
public interface Iterator {  
    public boolean hasNext();  
    public Object next();  
}
public interface Container {  
    public Iterator getIterator();  
}
public class NamesCollectoin implements Container{
public String name[]={"Sunaina Khan", "Ali khan","Noman","Shehzad Ali"};   
      
    public Iterator getIterator() {  
        return new CollectionofNamesIterate() ;  
    }  
    private class CollectionofNamesIterate implements Iterator{  
        int i;  
        @Override  
        public boolean hasNext() {  
            return i<name.length;  
        }  
        @Override  
        public Object next() {  
            if(this.hasNext()){  
                return name[i++];  
            }  
            return null;      
        }  
    }  
}
//Prototype Pattern
public interface Prototype {
    public Prototype getClone();
}
public class EmployeeRecord implements Prototype {

    private int id;
    private String name, designation;
    private double salary;
    private String address;
    
    public EmployeeRecord(int id, String name, String designation, double salary, String address) {

        this();
        this.id = id;
        this.name = name;
        this.designation = designation;
        this.salary = salary;
        this.address = address;
    }

    public EmployeeRecord() {
        System.out.println("   Employee Records   ");
        System.out.println("---------------------------------------------");
        System.out.println("Eid" + "\t" + "Ename" + "\t" + "Edesignation" + "\t" + "Esalary" + "\t\t" + "Eaddress");
    }

    public void showRecord() {

        System.out.println(id + "\t" + name + "\t" + designation + "\t" + salary + "\t" + address);
    }

    @Override
    public Prototype getClone() {

        return new EmployeeRecord(id, name, designation, salary, address);
    }
}